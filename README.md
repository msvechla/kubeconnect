# KubeConnect

*Wrapper to establish a connection to the Kubernetes API, either via Kubeconfig or in-cluster.*

KubeConnect is a generic wrapper that can be used both outside or inside a Kubernetes cluster to establish an API connection. In both cases a valid clientset is returned. No more boilerplate code.

## Getting Started

Refer to the following example on how to get started:

```go
package main

import (
    "log"

    "gitlab.com/msvechla/kubeconnect"
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func main() {
    clientSet, err := kubeconnect.Hybrid()

    if err != nil {
        log.Fatalf("Error establishing API connection: %s", err)
    }

    pods, _ := clientSet.CoreV1().Pods("default").List(metav1.ListOptions{})
    log.Println(pods)
}
```