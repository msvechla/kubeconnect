package kubeconnect

import (
	"fmt"
	"os"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

// Hybrid first tries to setup a connection via InCluster. On failure the connection is established via kubeconfig.
func Hybrid() (*kubernetes.Clientset, error) {
	clientSet, errInCluster := InCluster()
	if errInCluster != nil {
		clientSet, errViaKubeconfig := ViaKubeconfig()

		if errViaKubeconfig != nil {
			return nil, fmt.Errorf("Unable to establish a connection neither via kubeconfig nor in-cluster: %s / %s", errViaKubeconfig, errInCluster)
		}

		return clientSet, nil
	}
	return clientSet, nil
}

// InCluster establishes the in-cluster connection
func InCluster() (*kubernetes.Clientset, error) {
	// connect to k8s API from inside the cluster
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, fmt.Errorf("Unable to setup in-cluster connection, got error: %s", err)
	}

	// create the clientset
	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("Unable to setup in-cluster connection, got error: %s", err)
	}
	return clientSet, nil
}

// ViaKubeconfig establishes a local connection to the cluster
func ViaKubeconfig() (*kubernetes.Clientset, error) {

	kubeconfig := os.Getenv("KUBECONFIG")
	if kubeconfig == "" {
		return nil, fmt.Errorf("KUBECONFIG environment variable unset, unable to setup local connection")
	}

	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		return nil, fmt.Errorf("Unable to setup local connection, got error: %s", err)
	}

	// create the clientset
	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("Unable to setup local connection, got error: %s", err)
	}

	return clientSet, nil
}
